<?php

namespace Apicalypse\Enum;

/**
 * Class RequestTypes
 * @package Apicalypse\Enum
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
abstract class RequestTypes
{
    public const POST = 'POST';
    public const GET = 'GET';
}