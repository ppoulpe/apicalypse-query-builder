<?php

namespace Apicalypse\Enum;

/**
 * Class ComparatorsEnum
 * @package Apicalypse\Enum
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
abstract class Comparators
{
    /**
     *   = equals
     *   != not equals
     *   > is larger than
     *   >= is larger than or equal to
     *   < is smaller than
     *   <= is smaller than or equal to
     *   [] contains all of these values
     *   ![] does not contain all of these values
     *   () contains at least one of these values
     *   !() does not contain any of these values
     *   {} contains all of these values exclusively
     */

    public const EQUALS = "=";
    public const NOT_EQUALS = "!=";
    public const IS_LARGER_THAN = ">";
    public const IS_LARGER_OR_EQUAL_TO = ">=";
    public const IS_SMALLER_THAN = "<";
    public const IS_SMALLER_OR_EQUAL_TO = "<=";
}