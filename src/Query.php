<?php

namespace Apicalypse;

use Apicalypse\Enum\RequestTypes;
use Apicalypse\Criteria\Field;
use Apicalypse\Criteria\Limit;
use Apicalypse\Criteria\Search;
use Apicalypse\Criteria\Sort;
use Apicalypse\Criteria\Wheres;
use Apicalypse\Criteria\Where;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class Builder
 * @package ppoulpe\apicalypse
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
class Query
{
    private string $fields;
    private ?string $sort = null;
    private ?array $wheres = null;
    private ?string $where = null;
    private ?string $search = null;
    private ?string $limit = null;

    /**
     * @param string ...$field
     * @return Query
     */
    public function fields(string ...$field): Query
    {
        $this->fields = new Field($field);

        return $this;
    }

    /**
     * @param string ...$sort
     * @return Query
     */
    public function sort(string ...$sort): Query
    {
        $this->sort = new Sort($sort);

        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @param string|null $comparator
     * @return Query
     */
    public function where(
        string $key,
        $value,
        ?string $comparator = null
    ): Query
    {
        $this->wheres[] = new Wheres(
            $key,
            $value,
            $comparator
        );

        // We recompute for all new condition the final "where" clause.
        $this->where = new Where($this->wheres);

        return $this;
    }

    /**
     * @param string $search
     * @return Query
     */
    public function search(string $search): Query
    {
        $this->search = new Search($search);

        return $this;
    }

    /**
     * @param int $limit
     * @return Query
     */
    public function limit(int $limit): Query
    {
        $this->limit = new Limit($limit);

        return $this;
    }


    /**
     * @return string
     */
    public function getQuery(): string
    {
        return
            $this->search
            . $this->fields
            . $this->where
            . $this->limit
            . $this->sort;
    }

    /**
     * @param string $url
     * @param array|null $headers
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function request(
        string $url,
        ?array $headers = []
    ): string
    {
        $response = HttpClient::create()
            ->request(
                RequestTypes::POST,
                $url,
                [
                    'headers' => $headers,
                    'body' => $this->getQuery()
                ]
            );

        return $response->getContent();
    }

}