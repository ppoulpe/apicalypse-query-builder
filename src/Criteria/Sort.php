<?php

namespace Apicalypse\Criteria;

/**
 * Class Sort
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Sort
{
    /**
     * @var string[]
     */
    private array $sorting;

    /**
     * Sort constructor.
     * @param string[] $sorting
     */
    public function __construct(array $sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            "sort %s; ",
            implode(",", $this->sorting)
        );
    }
}