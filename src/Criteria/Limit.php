<?php

namespace Apicalypse\Criteria;

/**
 * Class Limit
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Limit
{
    /**
     * @var int
     */
    private int $limit;

    /**
     * Limit constructor.
     * @param int $limit
     */
    public function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            "limit %s; ",
            $this->limit
        );
    }
}