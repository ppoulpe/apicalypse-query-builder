<?php

namespace Apicalypse\Criteria;

/**
 * Class Field
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Field
{
    /**
     * @var string[]
     */
    private array $fields;

    /**
     * Field constructor.
     * @param string[] $fields
     */
    public function __construct(array $fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            "fields %s; ",
            implode(",", $this->fields)
        );
    }
}