<?php

namespace Apicalypse\Criteria;

use Apicalypse\Enum\Comparators;

/**
 * Class Where
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Wheres
{
    private string $key;
    private $value;
    private ?string $comparator;

    /**
     * Where constructor.
     * @param string $key
     * @param $value
     * @param string|null $comparator
     */
    public function __construct(
        string $key,
        $value,
        ?string $comparator = null
    )
    {
        $this->key = $key;
        $this->value = $value;
        $this->comparator = $comparator ?? Comparators::EQUALS;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            '%s %s %s',
            $this->key,
            $this->comparator,
            is_int($this->value) ? $this->value : '"' . $this->value . '"'
        );
    }
}