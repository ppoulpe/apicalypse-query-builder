<?php

namespace Apicalypse\Criteria;

/**
 * Class Wheres
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Where
{
    /**
     * @var string[]
     */
    private array $wheres;

    /**
     * Wheres constructor.
     * @param string[] $wheres
     */
    public function __construct(array $wheres)
    {
        $this->wheres = $wheres;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            "where %s; ",
            implode(",", $this->wheres)
        );
    }
}