<?php

namespace Apicalypse\Criteria;

/**
 * Class Search
 * @package Apicalypse\Criteria
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class Search
{
    /**
     * @var string
     */
    private string $search;

    /**
     * Search constructor.
     * @param string $search
     */
    public function __construct(string $search)
    {
        $this->search = $search;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            'search "%s"; ',
            $this->search
        );
    }
}