# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Add licence (MIT) 9b25a1d8
- Add changelog
- Add readme 2cfb7b30
- Adding first version of query builder (field, where, search, sort, limit) 5b571781

### Updated
- Improve unit tests 4a2e0561
- Improve documentation 573538d4

### Fixed
- Fix Exception namespace f57b0212