<?php

namespace UnitTest;

use Apicalypse\Query;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryTest
 * @package UnitTest
 * @author Jérôme Dumas <jerome.dumas@yahoo.fr>
 */
final class QueryTest extends TestCase
{
    public function testBuild_Successfully()
    {
        try {
            $query = (new Query())
                ->fields('title', 'summary')
                ->search('Tomb Raider')
                ->where('title','Tomb Raider')
                ->where('id', 1090)
                ->sort('title')
                ->limit(10);

            $this->assertEquals(
                'search "Tomb Raider"; fields title,summary; where title = "Tomb Raider",id = 1090; limit 10; sort title; ',
                $query->getQuery()
            );
        } catch (Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }
}