## APIcalypse query builder
Apicalypse is a query language used to obtain data from simple REST APIs.

This project aims to provide a set of methods to create and execute queries on APIs using the APIcalypse syntax.

To learn more about the syntax, visit https://apicalypse.io/.

## Installation
A `composer require` is used to retrieve the latest stable version of the project.
````shell script
composer require ppoulpe/apicalypse
````

## Usage
A set of methods allow you to create queries easily and quickly, making sure that the syntax is always correct.

Currently, the `fields`, `search`, `where`, `out` and `limit` filters are available. More will be added to the query builder as development progresses.

````php
use Apicalypse\Query;

$query = (new Query())
    ->fields('title', 'summary')
    ->search('Tomb Raider')
    ->where('title','Tomb Raider')
    ->where('id', 1090)
    ->sort('title')
    ->limit(10);

// Will echo : search "Tomb Raider"; fields title,summary; where title = "Tomb Raider",id = 1090; limit 10; sort title;
echo $query;
````

It's possible to use the embedded HTTP module and send the request directly to the API and retrieve the response.
````php
use Apicalypse\Query;

$formattedResponse = (new Query())
    ->fields('title', 'summary')
    ->search('Tomb Raider')
    ->where('title','Tomb Raider')
    ->where('id', 1090)
    ->sort('title')
    ->limit(10)
    ->request("https://my-awesome-api.com/games");

// Will echo : [{title: "Tomb Raider", summary: "This is a summary"}]
echo $formattedResponse;
````

## Unit tests
To run all the unit tests: 
````shell script
./vendor/bin/phpunit tests
````

